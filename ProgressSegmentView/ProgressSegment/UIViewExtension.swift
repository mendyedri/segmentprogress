//
//  UIViewExtension.swift
//  ProgressSegmentView
//
//  Created by Mendy Edri on 03/01/2019.
//  Copyright © 2019 LAB. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func setY(_ y: CGFloat) {
        frame = CGRect(x: frame.minX, y: y, width: frame.width, height: frame.height)
    }
    
    func setX(_ x: CGFloat) {
        frame = CGRect(x: x, y: frame.minY, width: frame.width, height: frame.height)
    }
    
    func setHeight(_ height: CGFloat) {
        frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: height)
    }
    
    func setWidth(_ width: CGFloat) {
        frame = CGRect(x: frame.minX, y: frame.minY, width: width, height: frame.height)
    }
}
