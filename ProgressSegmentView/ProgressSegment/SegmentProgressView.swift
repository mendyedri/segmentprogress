//
//  SegmentProgressView.swift
//  ProgressSegmentView
//
//  Created by Mendy Edri on 01/01/2019.
//  Copyright © 2019 LAB. All rights reserved.
//

import UIKit

class SegmentProgressView: UIView {

    fileprivate var sideMargins: Int {
       return 10 * 2
    }
    
    public var items: [ProgressItem] {
        didSet {
            drawEmpty()
        }
    }
    
    override public var frame: CGRect {
        didSet {
            drawEmpty()
        }
    }
    
    public var space: CGFloat {
        set {
            drawEmpty()
        }
        get {
            if items.count > 10 {
                return 3.0
            } else {
                return Configs.defaultSpace
            }
        }
    }
    
    public var trackColor: UIColor? = Configs.tintColor {
        didSet {
            drawEmpty()
        }
    }
    
    public var fillColor: UIColor? = Configs.fillColor {
        didSet {
            drawEmpty()
        }
    }
    
    private var progressViews = [ProgressView]()
    
    private var currentItem = 0
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(items: [ProgressItem]) {
        self.items = items
        super.init(frame: .zero)
    }
    
    fileprivate func drawEmpty() {
        if frame == CGRect.zero {
            return
        }
        let width = calulateWidth(container: frame.width, items: items.count, space: space, side: CGFloat(sideMargins))
        
        let stackView = UIStackView.init(frame: self.bounds)
        stackView.setX(CGFloat(sideMargins / 2))
        stackView.setWidth(self.bounds.width - CGFloat(sideMargins))
        stackView.axis = .horizontal
        stackView.spacing = space
        stackView.distribution = .fillEqually
        addSubview(stackView)
        
        for item in items {
            let progressView = ProgressView.init(trackColor: trackColor, fillColor: fillColor, item: item, viewSize: CGSize(width: Int(width), height: Int(frame.height)))
            progressViews.append(progressView)
            progressView.delegate = self
            stackView.addArrangedSubview(progressView)
        }
    }
    
    fileprivate func calulateWidth(container width: CGFloat, items count: Int, space: CGFloat, side margins: CGFloat) -> CGFloat {
        return (width - margins - space * CGFloat(count-2)) / CGFloat(count)
    }
    
}

extension SegmentProgressView: ProgressViewProtocol {
    func animationDidEnd() {
        currentItem += 1
        resume()
    }
}

extension SegmentProgressView {
    func pause() {
        progressViews[currentItem].pause()
    }
    
    func resume() {
        progressViews[currentItem].resume()
    }
    
    func next() {
        setAsFull(item: progressViews[currentItem])
        currentItem += 1
        resume()
    }
    
    func previous() {
        setAsEmpty(item: progressViews[currentItem])
        currentItem -= 1
        setAsEmpty(item: progressViews[currentItem])
        resume()
    }
    
    fileprivate func setAsFull(item: ProgressView) {
        item.setToFull()
    }
    
    fileprivate func setAsEmpty(item: ProgressView) {
        item.setToEmpty()
    }
}

