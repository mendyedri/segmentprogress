//
//  ProgressView.swift
//  ProgressSegmentView
//
//  Created by Mendy Edri on 01/01/2019.
//  Copyright © 2019 LAB. All rights reserved.
//

import UIKit

protocol ProgressViewProtocol {
    func animationDidEnd()
}

class ProgressView: UIView {
    
    var trackColor: UIColor!
    var fillColor: UIColor!
    
    var progress: ProgressItem!
    
    var animationView = UIView.init()
    
    var animator = UIViewPropertyAnimator()
    
    var delegate: ProgressViewProtocol?
    
    var forced: Bool = false
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(trackColor: UIColor?, fillColor: UIColor?, item: ProgressItem, viewSize: CGSize) {
        let viewFrame = CGRect(x: 0, y: 0, width: viewSize.width, height: viewSize.height)
        super.init(frame: viewFrame)
        self.trackColor = trackColor ?? Configs.tintColor
        self.fillColor = fillColor ?? Configs.fillColor
        self.progress = item
        self.viewSetup()
        self.frame = viewFrame
    }
    
    private func viewSetup() {
        self.layer.cornerRadius = self.frame.height / 2
        self.backgroundColor = self.trackColor
        
        setupAnimator()
    }
    
    fileprivate func setupAnimator() {
        animationView.frame = self.bounds
        animationView.setWidth(0)
        animationView.backgroundColor = self.fillColor
        animationView.layer.cornerRadius = self.frame.height / 2
        addSubview(animationView)
        setupAnimationAndCompletion()
    }
    
    fileprivate func setupAnimationAndCompletion() {
        animator = UIViewPropertyAnimator.init(duration: TimeInterval(progress.duration), curve: .linear, animations: {
            self.animationView.setWidth(self.frame.width)
        })
        animator.addCompletion { (position) in
            if (position == .end && self.forced == false) {
                self.delegate?.animationDidEnd()
            }
            self.forced = false
        }
    }
}

extension ProgressView {
    func pause() {
        animator.pauseAnimation()
    }
    
    func resume() {
        animator.startAnimation()
    }
    
    func setToFull() {
        animator.pauseAnimation()
        forced = true
        animator.fractionComplete = 1
    }
    
    func setToEmpty() {
        animator.pauseAnimation()
        forced = true
        animator.fractionComplete = 0
    }
}
