//
//  ProgressItem.swift
//  ProgressSegmentView
//
//  Created by Mendy Edri on 01/01/2019.
//  Copyright © 2019 LAB. All rights reserved.
//

import Foundation
import UIKit

struct ProgressItem {
    
    let duration: CGFloat
}
