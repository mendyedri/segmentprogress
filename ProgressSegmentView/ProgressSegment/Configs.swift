//
//  Configs.swift
//  ProgressSegmentView
//
//  Created by Mendy Edri on 01/01/2019.
//  Copyright © 2019 LAB. All rights reserved.
//

import Foundation
import UIKit

struct Configs {
    static let defaultSpace: CGFloat = 5.0
    static let tintColor: UIColor = UIColor.lightGray
    static let fillColor: UIColor = UIColor.black
}
