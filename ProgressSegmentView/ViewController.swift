//
//  ViewController.swift
//  ProgressSegmentView
//
//  Created by Mendy Edri on 31/12/2018.
//  Copyright © 2018 LAB. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var segmentView: SegmentProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    fileprivate func setup() {
        var items: [ProgressItem] = []
        
        for _ in 0..<9 {
            let progress = ProgressItem.init(duration: 5)
            items.append(progress)
        }
        
        segmentView = SegmentProgressView(items: items)
        segmentView.frame = CGRect(x: 0, y: 300, width: UIScreen.main.bounds.width, height: 5)
        self.view.addSubview(segmentView)
        
        segmentView.backgroundColor = UIColor.yellow
    }
}

extension ViewController {
    @IBAction func play() {
        segmentView.resume()
    }
    
    @IBAction func stop() {
        segmentView.pause()
    }
    
    @IBAction func next() {
        segmentView.next()
    }
    
    @IBAction func previous() {
        segmentView.previous()
    }
}

